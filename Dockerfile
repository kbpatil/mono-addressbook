# Use Tomcat as base image
FROM tomcat:9.0-jdk8

# Remove existing ROOT application
RUN rm -rf /usr/local/tomcat/webapps/ROOT

# Copy the .war artifact to the Tomcat webapps directory
COPY target/*.war /usr/local/tomcat/webapps/ROOT.war

# Expose the Tomcat port
EXPOSE 8080

# Start Tomcat
CMD ["catalina.sh", "run"]

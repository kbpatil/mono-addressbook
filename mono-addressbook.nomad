# Define a job named "addressbook"
job "addressbook" {
  # Specify the datacenters where this job should run (["dc1"] in this case)
  datacenters = ["dc1"]

  # Define a job group named "addressbook"
  group "addressbook" {
    # Run a single instance of this task (count = 1)
    count = 1

    # Network configuration for the task group
    network {
      # Expose a port named "http" and map it to host port 8080
      port "http" {
        to = 8080
      }
    }

    # Define a task named "web" 
    task "web" {
      # Use the "docker" driver to run a Docker container
      driver = "docker"

      # Configuration for the Docker container
      config {
        # Use the latest version of the "registry.gitlab.com/kbpatil/mono-addressbook" image
        image = "registry.gitlab.com/kbpatil/mono-addressbook:latest"
        # Expose the container's port named "http"
        ports = ["http"]
      }

      # Resource allocation for the task
      resources {
        # Allocate 50 CPU units
        cpu = 50
        # Allocate 256 MB of memory
        memory = 256
      }
    }
  }
}
